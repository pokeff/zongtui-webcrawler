/**
 * Project Name:webcrawler-sourceer
 * File Name:IData.java
 * Package Name:com.zongtui.webcrawler
 * Date:2015年4月11日下午11:41:49
 * Copyright (c) 2015, 众推项目组版权所有.
 *
 */
package com.zongtui.webcrawler;

/**
 * ClassName: IData <br/>
 * Function: 抽象数据产品. <br/>
 * date: 2015年4月11日 下午11:41:49 <br/>
 *
 * @author zhangfeng
 * @version 
 * @since JDK 1.7
 */
public abstract class IData {
	
	/**
	 * name:数据产品名.
	 */
	public String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
