/**
 * Project Name:webcrawler-sourceer
 * File Name:SourceDirector.java
 * Package Name:com.zongtui.webcrawler
 * Date:2015年4月12日上午12:03:34
 * Copyright (c) 2015, 众推项目组版权所有.
 *
 */
package com.zongtui.webcrawler;

/**
 * ClassName: SourceDirector <br/>
 * Function: 指挥数据生成方式. <br/>
 * date: 2015年4月12日 上午12:03:34 <br/>
 *
 * @author zhangfeng
 * @version 
 * @since JDK 1.7
 */
public abstract class SourceDirector {
	
	private ISourceBuilder dataBuilder;
	
	public SourceDirector(ISourceBuilder sourceBuilder){
        this.dataBuilder = sourceBuilder;
    }
	
	/**
	 * construct:构造数据从选择的方式. <br/>
	 *
	 * @author zhangfeng
	 * @since JDK 1.7
	 */
	public void construct(){
		dataBuilder.buildSourceByCrawler();
	}

}
