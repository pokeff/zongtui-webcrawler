/**
 * Project Name:webcrawler-sourceer
 * File Name:DataBuilder.java
 * Package Name:com.zongtui.webcrawler
 * Date:2015年4月11日下午11:54:01
 * Copyright (c) 2015, 众推项目组版权所有.
 *
 */
package com.zongtui.webcrawler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zongtui.webcrawler.sourceer.ResultItems;
import com.zongtui.webcrawler.sourceer.Spider;
import com.zongtui.webcrawler.sourceer.crawler.BaiduBaikePageProcessor;

/**
 * ClassName: DataBuilder <br/>
 * Function: 数据构造实现. <br/>
 * date: 2015年4月11日 下午11:54:01 <br/>
 *
 * @author Administrator
 * @version 
 * @since JDK 1.7
 */
public class CrawlerDataBuilder implements ISourceBuilder {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private IData resultItems = new ResultItems();

	/**
	 * 由爬虫够着数据源.
	 * @see com.zongtui.webcrawler.ISourceBuilder#buildSourceByCrawler()
	 */
	@Override
	public void buildSourceByCrawler() {
//		System.out.println("由爬虫构造数据……");
		Spider spider = Spider.create(new BaiduBaikePageProcessor()).thread(1);
		String urlTemplate = "http://baike.baidu.com/search/word?word=%s&pic=1&sug=1&enc=utf8";
        resultItems = spider.<ResultItems>get(String.format(urlTemplate, "水力发电"));
        spider.close();
	}

	/**
	 * 由文件构造数据.
	 * @see com.zongtui.webcrawler.ISourceBuilder#buildSourceByFile()
	 */
	@Override
	public void buildSourceByFile() {
		System.out.println("由文件构造数据……");
	}

	/**
	 * 由ftp构造数据.
	 * @see com.zongtui.webcrawler.ISourceBuilder#buildSourceByFtp()
	 */
	@Override
	public void buildSourceByFtp() {
		System.out.println("由ftp构造数据……");
	}

	/**
	 * 得到结果.
	 * @see com.zongtui.webcrawler.ISourceBuilder#retrieveResult()
	 */
	@Override
	public IData retrieveResult() {
//		System.out.println("生成爬虫数据产品");
		return resultItems;
	}

}
