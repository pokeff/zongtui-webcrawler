#zongtui-webcrawler

众推，开源版的今日头条！

基于hadoop思维的分布式网络爬虫。

目前已经把webmagic的基础部分加入到工程中，实现简单分布式。

想参与加入的可以入群：众推 194338168

项目中所用的编译版本为JDK1.7。

大数据博客整理：
https://github.com/jxqlovejava/PopularBlogSites/blob/master/README.md
